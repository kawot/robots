﻿using UnityEngine;
using System.Collections;

public class Radar : MonoBehaviour, IPart
{
    [SerializeField] float radius = 10f;

    public delegate void RadarEnter(Collider other);
    public RadarEnter OnRadarEnter;
    public delegate void RadarExit(Collider other);
    public RadarEnter OnRadarExit;

    private AI cpu;
    private SphereCollider collider;

    public void Init(AI cpu)
    {
        this.cpu = cpu;
        cpu.AddAction(ActionType.Scout);
        collider = GetComponent<SphereCollider>();
        collider.enabled = false;
        collider.radius = radius;
    }

    public void TurnOn (bool isOn = true)
    {
        collider.enabled = isOn;
    }

    private void OnTriggerEnter(Collider other)
    {
        if (OnRadarEnter != null) OnRadarEnter(other);
    }

    private void OnTriggerExit(Collider other)
    {
        if (OnRadarExit != null) OnRadarExit(other);
    }
}
