﻿using UnityEngine;
using System.Collections;

public class FuelTank : MonoBehaviour, IPart
{
    [SerializeField] float fuelCapacity = 10f;

    private AI cpu;
    private float fuel;
    private bool lowFuel = false;

    public void Init(AI cpu)
    {
        this.cpu = cpu;
        cpu.AddAction(ActionType.Return);
        Refuel();
    }

    public void BurnFuel (float amount)
    {
        fuel -= amount;
        if (fuel < fuelCapacity * 0.5f && !lowFuel)
        {
            Debug.Log("running of fuel");
            lowFuel = true;
            cpu.ChangeActionValue(ActionType.Return, 1000f);
        } 
    }

    private void Refuel()
    {
        fuel = fuelCapacity;
        if (lowFuel)
        {
            Debug.Log("fuel refilled");
            lowFuel = false;
            cpu.ChangeActionValue(ActionType.Return, -1000f);
        }
    }
}
