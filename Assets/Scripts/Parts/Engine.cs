﻿using UnityEngine;
using System.Collections;

public class Engine : MonoBehaviour, IPart
{
    [SerializeField] float speed = 1f;
    [SerializeField] float turnRate = 180f;

    private Vector3 targetPosition = Vector3.zero;
    private Transform targetObject = null;
    private bool targetIsStatic = true;
    private bool isActive = false;
    private Transform body;

    private FuelTank fuelTank;

    public void Init(AI cpu)
    {
        body = cpu.transform;
        fuelTank = transform.parent.GetComponentInChildren<FuelTank>();
        if (!fuelTank) Debug.LogError("There is no fuel tank for engine " + name + " of " + transform.parent.gameObject.name);
    }

    public void SetTarget(Vector3 pos)
    {
        targetObject = null;
        targetPosition = pos;
        targetIsStatic = true;
        Debug.Log("Got new target : " + pos.ToString());
    }

    public void SetTarget(Transform target)
    {
        targetObject = target;
        targetPosition = target.position;
        targetIsStatic = false;
        Debug.Log("Got new target : " + target.ToString());
    }

    public void Run()
    {
        isActive = true;
        Debug.Log("Engine is active");
    }

    public void Stop()
    {
        isActive = false;
    }

    private void Update()
    {
        if (!isActive) return;

        Vector3 target;
        if (targetIsStatic) target = targetPosition;
        else target = targetObject.position;
        var dir = target - body.position;
        var angle = Vector3.SignedAngle(body.forward, dir, Vector3.up);
        var deltaTurn = turnRate * Time.deltaTime;
        angle = Mathf.Clamp(angle, -deltaTurn, deltaTurn);
        body.Rotate(Vector3.up, angle);

        body.Translate(Vector3.forward * speed * Time.deltaTime);

        fuelTank.BurnFuel(Time.deltaTime);
    }
}
