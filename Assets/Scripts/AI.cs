﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AI : MonoBehaviour
{
    private Engine engine;
    private Radar radar;
    private List<AIAction> actions = new List<AIAction>();
    private AIAction currentAction = null;

    public Radar GetRadar()
    {
        return radar;
    }

    public void InitiateParts()
    {
        radar = GetComponentInChildren<Radar>();
        if (!radar) Debug.LogError(gameObject.name + "'s AI got no radar");
        engine = GetComponentInChildren<Engine>();
        if (!engine) Debug.LogError(gameObject.name + "'s AI got no engine");

        var parts = GetComponentsInChildren<IPart>();
        for (var i = 0; i < parts.Length; i++)
        {
            parts[i].Init(this);
        }

        radar.TurnOn();
        engine.Run();

        currentAction = GetActionOfType(ActionType.Scout);
        Reevaluate();
    }

    public void AddAction(ActionType type)
    {
        for (var i = 0; i < actions.Count; i++)
        {
            if (actions[i].type == type) return;
        }
        actions.Add(new AIAction(type));
    }

    public void ChangeActionValue (ActionType type, float valueChange)
    {
        GetActionOfType(type).value += valueChange;
        Reevaluate();
    }

    private void Start()
    {
        InitiateParts();
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Q))
        {
            Debug.Log("Current action is " + currentAction.type.ToString());
            for (var i = 0; i < actions.Count; i++)
            {
                Debug.Log(actions[i].type.ToString() + " : " + actions[i].value);
            }
        }
    }

    private void Reevaluate()
    {
        if (currentAction == null)
        {
            Debug.Log("No actions for AI of " + gameObject.name);
            return;
        }

        for (var i = 0; i < actions.Count; i++)
        {
            if (actions[i].value > currentAction.value) currentAction = actions[i];
        }
        Retarget();
    }

    private void Retarget()
    {
        switch (currentAction.type)
        {
            case ActionType.Return:
                engine.SetTarget(Vector3.zero);
                break;
            case ActionType.Scout:
                var rndCircle = Random.insideUnitCircle;
                engine.SetTarget(10f * new Vector3(rndCircle.x, 0f, rndCircle.y));
                break;
            case ActionType.Gather:
                Debug.Log("there is nothing to gather yet");
                currentAction = GetActionOfType(ActionType.Scout);
                break;
            default:
                Debug.Log("There is no action " + currentAction.type.ToString());
                break;
        }
    }

    private AIAction GetActionOfType(ActionType type)
    {
        for (var i = 0; i < actions.Count; i++)
        {
            if (actions[i].type == type)
            {
                return actions[i];
            }
        }
        Debug.LogError("action of type " + type + " is not found");
        return null;
    }
}
