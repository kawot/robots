﻿using UnityEngine;
using System.Collections;

public class AIAction
{
    public ActionType type;
    public float value;

    public AIAction(ActionType type, float value = 0f)
    {
        this.type = type;
        this.value = value;
    }
}

public enum ActionType
{
    Return,
    Scout,
    Gather
}
